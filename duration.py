from pandas import *
from datetime import datetime
from tabulate import tabulate

# Set up variables for name of csv and names of request ID, time, and event type columns from csv
csv_name = 'test.csv'
rid_col = 'requestId'
time_col = '_time'
event_type_col = 'interactionIdentifier'

# Read in test csv as a dataframe, filter data to columns, create list of request IDs, create list of unique request IDs
data = read_csv(csv_name)
filtered_data = data.filter([rid_col, event_type_col, time_col])
rid_list = data[rid_col]
rid_unique = list(set(rid_list))
list_of_lists = []

for rid in rid_unique:
    # Create a dataframe for each unique request ID
    filtered_by_rid = filtered_data[filtered_data.requestId.str.contains(rid)]
    filtered_by_rid = filtered_by_rid.fillna('')

    # Sort by event types ascending and grab the top result (there will only be one result and some blanks)
    sorted_event = filtered_by_rid.sort_values(by=[event_type_col], ascending=False)
    event_type = sorted_event.iloc[0, sorted_event.columns.get_loc(event_type_col)]

    # Sort by time and grab the earliest result.  Convert to ms for use later.
    sorted_time = filtered_by_rid.sort_values(by=[time_col])
    start_time_str = sorted_time.iloc[0, sorted_time.columns.get_loc(time_col)]
    start_time = datetime.strptime(start_time_str, '%Y-%m-%dT%H:%M:%S.%f+0000')
    start_time_ms = start_time.timestamp() * 1000

    # Sort by time desc and grab the latest result.  Convert to ms for use later.
    sorted_time_desc = filtered_by_rid.sort_values(by=[time_col], ascending=False)
    end_time_str = sorted_time_desc.iloc[0, sorted_time_desc.columns.get_loc(time_col)]
    end_time = datetime.strptime(end_time_str, '%Y-%m-%dT%H:%M:%S.%f+0000')
    end_time_ms = end_time.timestamp() * 1000

    # Organize results into a list of lists for use in final dataframe
    indiv_list = [rid, event_type, start_time, end_time, int(end_time_ms - start_time_ms)]
    list_of_lists.append(indiv_list)

# Define final dataframe
headers=['requestId', 'Event Type', 'Start Time', 'End Time', 'Time Diff (ms)']
df = DataFrame(list_of_lists, columns=headers)
time_diff_avg = df['Time Diff (ms)'].mean()
time_diff_min = df['Time Diff (ms)'].min()
time_diff_max = df['Time Diff (ms)'].max()

print('Average Time Diff (ms): ' + str(df['Time Diff (ms)'].mean()))
print('Min Time Diff (ms): ' + str(df['Time Diff (ms)'].min()))
print('Max Time Diff (ms): ' + str(df['Time Diff (ms)'].max()))
print('\n')
print('All Request IDs:')
print(tabulate(df, headers=headers, tablefmt='simple'))
