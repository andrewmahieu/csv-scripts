# CSV scripts



## Getting started

Using duration script:
- `pip install pandas`
- `pip install tabulate`
- When running locally, it is easier to store csv for analysis in the same folder as the script itself. 
- Update "variables" section of script to match csv name and column names in your csv
- To run, just run `python duration.py` from cmd line in folder where script resides
